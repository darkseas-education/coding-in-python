class Check:
    
    def __init__(self, expected, result=None):
        self.expected = expected
        self.result = result
        self.single = False
        self.match = None
        if result is None:
            self.single = True
            self.match = bool(expected)
        else:
            self.match = expected == result
        
    def _repr_html_(self):
        if self.match:
            return self._success()
        else:
            return self._fail()
        
    def _fail(self):
        if self.single:
            s = """
            <div class="alert alert-danger">
            {} <br/>
            Not OK
            </div>
            """.format(repr(self.expected))
        else:
            s = """
            <div class="alert alert-danger">
            Expected     {} <br/>
            but received {}
            </div>
            """.format(repr(self.expected), repr(self.result))
        return s
        
    def _success(self):
        s = """
        <div class="alert alert-success">
        Both are {} <br/>
        OK
        </div>
        """.format(repr(self.result))
        return s    
    
ok = Check