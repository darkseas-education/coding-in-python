![Build Status](https://gitlab.com/pages/plain-html/badges/plain/build.svg)

---

Coding with Python

Welcome to repo for Duncraig IT's Coding with Python notebook series.

This is a set of Jupyter notebooks that lets you try Python in your browser.
In fact, the Python code is actually running on **your** machine in your browser, so I recommend a modern browser that support WebAssembly. This means please use an up-to-date version of Firefox or Chrome.

## Why use notebooks?

Jupyter notebooks lets you read and write text and code together. You can see the output of your code straight away (like in the shell) but also edit and change your code without having to type it all again (like in a script).

## Why use this set of notebooks?

These notebooks are built on JupyterLite, which means there is no server to maintain or code to install. Everything runs in your browser, and can be saved in the local storage of your browser so that it will be there for you next time.

## Getting started

You can find the notebooks by clicking [this link](https://darkseas-education.gitlab.io/coding-in-python/) and then finding the notebook called `00-index.ipynb`.

From there you can move to the first in the series `01-hello-world.ipynb` to introduce you to notebooks and Python.


---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
