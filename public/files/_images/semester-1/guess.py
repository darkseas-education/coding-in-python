import random

print('Welcome to my Quiz!')
print('Enter -1 to quit')

score = 0
correct_answer = 0
num1 = 0
num2 = 0
ans = 0

while ans != -1:
    print('\n')
    num1 = random.randint(1, 10)
    num2 = random.randint(1, 10)
    correct_answer = num1 * num2
    question = 'What is ' + str(num1) + ' x ' + str(num2) + '?: '
    ans = int(input(question))

    if ans == correct_answer:
        print('Correct Answer!')
        score = score + 1

print('\n')
print('Goodbye, Your final score is: ', score)
